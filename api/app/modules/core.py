from http import HTTPStatus

from flask import Blueprint, jsonify

# Setup the Blueprint
core = Blueprint('core', __name__)

@core.route('/api', methods=['GET'])
def home():
    return jsonify(msg="API-boilerplate is all set ! Yay !"), HTTPStatus.OK