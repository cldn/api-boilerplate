from flask_security import RoleMixin, UserMixin
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

roles_users = db.Table('roles_users',
    db.Column('user_id', db.Integer, db.ForeignKey('T_USER.id'), primary_key=True),
    db.Column('role_id', db.Integer, db.ForeignKey('T_ROLE.id'), primary_key=True)
)

class Role(db.Model, RoleMixin):
    __tablename__ = 'T_ROLE'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    description = db.Column(db.String)

    @property
    def to_dict(self):
        return {'id': self.id, 'name': self.name, 'description': self.description}


class User(db.Model, UserMixin):
    __tablename__ = 'T_USER'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    active = db.Column(db.Boolean)
    roles = db.relationship('Role', secondary=roles_users, backref=db.backref('users', lazy=True))

    @property
    def to_dict(self):
        return {'id': self.id, 'email': self.email, 'roles': [role.to_dict for role in self.roles]}
