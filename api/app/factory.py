import logging

from flask import Flask
from flask_security import Security, SQLAlchemyUserDatastore
from flask_sqlalchemy import SQLAlchemy

from .config import CONFIG
from .modules.models import Role, User, db

def create_app(config_name):
    # Flask initialization
    app = Flask(__name__)
    app.config.from_object(CONFIG[config_name])

    # Create the database connection object
    db.init_app(app)

    # Setup Flask-Security
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security = Security(app, user_datastore)
    
    return app, user_datastore, security

def prepare_db(app, user_datastore):
    logging.info("Preparing the database...")
    db.create_all()
    if not User.query.first():
        default_user = app.config['DEFAULT_APP_USER']
        user_datastore.create_user(email=default_user['email'], password=default_user['password'])
    db.session.commit()
