from os import getenv

from .factory import create_app, prepare_db
from .modules.core import core

# Setup Flask
environment = getenv('FLASK_CONFIGURATION', 'development')
app, user_datastore, security = create_app(environment)

@app.before_first_request
def prepare_app():
    prepare_db(app, user_datastore)

app.register_blueprint(core)
