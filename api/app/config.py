"""Configuration file for the different backend application config options"""

CONFIG = {
    "development": "app.config.DevelopmentConfig",
    "testing": "app.config.TestingConfig",
    "production": "app.config.ProductionConfig",
    "default": "app.config.DevelopmentConfig"
}

class BaseConfig(object):
    """Base class for default set of configs"""
    DEBUG = False
    TESTING = False
    SECRET_KEY = '\xee4\xe9\xd7\x9a\xf4b\x0b\x9d\xb9\xdbQ\xb8nf\xd3j\x87\xd4\xa2\x95\xc7\xcf\x94'
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    DEFAULT_APP_USER = {'email': 'admin@replace.me', 'password': 'admin' }

class DevelopmentConfig(BaseConfig):
    """Default set of configurations for a development environment"""
    DEBUG = True
    TESTING = False

class ProductionConfig(BaseConfig):
    """Default set of configurations for a production environment"""
    DEBUG = False
    TESTING = False

class TestingConfig(BaseConfig):
    """Default set of configurations for a testing environment"""
    DEBUG = False
    TESTING = True
