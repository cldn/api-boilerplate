"""Entry point for the backend app"""

from app.server import app

if __name__ == '__main__':
    try:
        print("Booting API..")
        app.run(host='127.0.0.1', port=8000, debug=True)
    except KeyboardInterrupt:
        print('shutting down..')
        exit(0)
    except Exception as exc:
        print(exc)
